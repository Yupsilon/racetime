# RaceTime Continuum #

Race Time Continuum is a top down racing game where up to 4 racers have to avoid randomly generated obstacles. At the end of the lap, they travel back in time and have to race alongside their past-selves in a race that gets more complicated the longer you play. Have fun!

This game was made as a hobbyist project for the Game Development World Championship, in less than a week.

Download and play directly at: https://ypsilon.itch.io/race-time-continuum

# Installation #

To install, simply place all the files in a new Unity project. The project was last built with 2021 Unity.