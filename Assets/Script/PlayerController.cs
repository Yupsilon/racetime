using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PlayerController : MonoBehaviour
{
    public static Color[] playercolors = new Color[]
        {
            Color.red,
            Color.blue,
            Color.green,
            Color.yellow
        };


    float hSpeed = 6;
    float sSpeed = .05f;
    float steering = .5f;
    bool registermovement = false;

    public int playerID = 0;
    protected float SpawnTime = 0;
    protected Color MyColor = Color.white;

    public bool defeated = false;
    public Vector2 input = Vector2.zero;
    public int recordstate = 0;
    public List<Vector3> History = new List<Vector3>();
    
    protected SpriteRenderer Display;
    protected Collider2D Collision;
    protected Rigidbody2D Body;
    protected Animator Animations;
    protected ParticleSystem Trail;
    protected virtual void Awake()
    {
        Display = GetComponent<SpriteRenderer>();
        Collision = GetComponent<Collider2D>();
        Body = GetComponent<Rigidbody2D>();
        Trail = GetComponent<ParticleSystem>();
        Animations = GetComponent<Animator>();
    }
    protected virtual void OnEnable()
    {
        Display.enabled = true;
        Collision.enabled = true;
        registermovement = true;
        Trail.Play();
        Body.position = Camera.main.transform.position;
        StartCoroutine(SpawnIntoWorld());
    }
    private void OnDisable()
    {
        Display.enabled = false;
        Collision.enabled = false;
        Trail.Stop();
        StopCoroutine(SpawnIntoWorld());
    }
    protected virtual IEnumerator SpawnIntoWorld()
    {
        MyColor = playercolors[playerID];
        recordstate = 0;
        Collision.enabled = false;
        Animations.SetFloat("steer", .5f);

        yield return new WaitForSeconds(GameController.SetupTime);
        Collision.enabled = true;
        SpawnTime = Time.time;
        History.Clear();
        History.Add(new Vector3(transform.position.x, transform.position.y, 0));
        History.Add(new Vector3(input.x, input.y, 0));
        recordstate = 1;
        Display.color = MyColor;
        FTL(true);
    }
    public void EndGame()
    {
        StartCoroutine(ExitWorld());
    }
    IEnumerator ExitWorld()
    {
        if (!defeated)
        {
            Collision.enabled = false;
            if (recordstate == 1)
            {
                History.Add(new Vector3(0, 0, Time.time - SpawnTime));
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(0, GameController.FinishTime - 1));

            registermovement = false;
            FTL(false);
            Color rendercolor = Display.color;
            while (Display.color.a > 0)
            {
                rendercolor.a -= Time.deltaTime * 3;
                Display.color = rendercolor;
                input = Vector2.zero;
                yield return new WaitForEndOfFrame();

            }
        }
        enabled = false;
    }
    protected virtual void Update()
    {
        if (!registermovement)
        {
            return;
        }

            Vector2 newinput = new Vector2(
                Input.GetAxisRaw((playerID > 0 ? ("Player " +(playerID + 1) + " ") : "")+"Horizontal"),
                Input.GetAxisRaw((playerID > 0 ? ("Player " + (playerID + 1) + " ") : "") + "Vertical")
                );

        if (recordstate == 0)
        {
            if (Time.time % .2f < .1f)
            {
                Display.color = MyColor;
            }
            else
            {
                Display.color = Color.clear;
            }
        }
        else if ((newinput.x != input.x || newinput.y != input.y))
        {
            History.Add(new Vector3(newinput.x, newinput.y, Time.time - SpawnTime));
        }
        input = newinput;
        
    }
    private void FixedUpdate()
    {
        Vector2 velocity = input.normalized * hSpeed;
        Vector2 stageBounds = new Vector2(Camera.main.orthographicSize * Camera.main.aspect - .5f, Camera.main.orthographicSize - .5f);

        if (input.x < 0)
        {
            steering -= sSpeed;
        }
        else if (input.x > 0 )
        {
            steering += sSpeed;
        }
        else
        {
            steering += steering == .5f ? 0 : (steering < .5f ? sSpeed : -sSpeed);
        }
        steering = Mathf.Clamp(steering, 0, 1);
        Animations.SetFloat("steer", steering);

        if (Body.position.x+ velocity.x*Time.deltaTime < -stageBounds.x || Body.position.x + velocity.x * Time.deltaTime > stageBounds.x)
        {
            velocity.x = 0;
        }
        if (Body.position.y + velocity.y * Time.deltaTime < -stageBounds.y || Body.position.y + velocity.y * Time.deltaTime > stageBounds.y)
        {
            velocity.y = 0;
        }
        Body.velocity = velocity;
    }
    public virtual void Kill()
    {
        if (!registermovement)
        {
            return;
        }
        defeated = true;
        enabled = false;
        registermovement = false;
        History.Add(new Vector3(input.x, input.y, Time.time - SpawnTime));
        History.Add(new Vector3(0, 0, -1));
        Explode();
        GameController.main.CheckGameState();
    }
    public void Explode()
    {
        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/Explosion"));
        poof.transform.position = transform.position;
    }
    public void FTL(bool reverse)
    {
        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/FTL"));
        poof.transform.SetParent(transform);
        poof.transform.localPosition =  Vector3.back;
        if (reverse)
        {
            poof.transform.rotation = Quaternion.Euler(0, 0, 180);
            poof.GetComponent<AudioSource>().volume = 1f / GameController.main.GetNPlayers();
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Kill" || collision.collider.tag == "Player")
        {
            Kill();
        }
    }
}
