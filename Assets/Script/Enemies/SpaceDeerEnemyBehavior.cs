using UnityEngine;

public class SpaceDeerEnemyBehavior : EnemyDirectionalBehavior
{
    bool strongjump = false;
    protected override void Initialize()
    {
        base.Initialize();

        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/EnemyWelcome"));
        poof.transform.position = new Vector2(
             (Camera.main.aspect * Camera.main.orthographicSize) * ChargeDirection,
             Camera.main.orthographicSize * Data * .5f
            );
        poof.transform.localScale = transform.localScale;
        poof.transform.rotation = Quaternion.Euler(0, 0, 90 * ChargeDirection);
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             (Camera.main.aspect * Camera.main.orthographicSize + 1f) * ChargeDirection,
             Camera.main.orthographicSize * Data * .5f,
             transform.position.z
            );

        base.Spawn();
        awaketime = Time.time;
        strongjump = false;
        velocity.y = 0;
    }

    protected override float GetLifeTime()
    {
        return 6f;
    }

    void FixedUpdate()
    {
        if (active)
        {
            velocity.x = 4 * -ChargeDirection;
               velocity.y -= 4 * Time.deltaTime;

            if (velocity.y<-3)
            {
                velocity.y = strongjump ? 5 : 3;
                strongjump = !strongjump;
            }

            Body.position += velocity * Time.deltaTime;
        }
    }
    public override float GetCost()
    {
        return 1.1f;
    }
}