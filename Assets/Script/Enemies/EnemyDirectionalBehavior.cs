﻿using UnityEngine;

public abstract class EnemyDirectionalBehavior : EnemyBehavior
{
    public int ChargeDirection = 1;
    protected float awaketime = 0f;
    protected override void Initialize()
    {
        base.Initialize();
        
    }
    protected override void Spawn()
    {
        base.Spawn();
        awaketime = Time.time;
    }
}