using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedsterEnemyBehavior : EnemyBehavior
{
    protected override void Initialize()
    {
        base.Initialize();

        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/EnemyWelcome"));
        poof.transform.position = new Vector2(
            Camera.main.aspect * Camera.main.orthographicSize * Data,
            -Camera.main.orthographicSize
            );
        poof.transform.localScale = transform.localScale;
        poof.transform.rotation = Quaternion.Euler(0, 0, 0 );
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             Camera.main.aspect * Camera.main.orthographicSize * Data,
             -Camera.main.orthographicSize - 2,
             transform.position.z
            );

        base.Spawn();
        GetComponent<ParticleSystem>().Play();
    }
    public override float GetCost()
    {
        return 1.2f; 
    }
    protected override float GetLifeTime()
    {
        return 3f;
    }
}
