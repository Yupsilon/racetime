﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaveController", menuName = "ScriptableObjects/WaveController", order = 1)]
public class WaveController : ScriptableObject
    {
    public string[] WaveData;
    public string[] Wave()
    {
        return Wave(Random.Range(0, WaveData.Length-1));
    }
    public string[] Wave(int random)
    {

        string sWave = WaveData[random];
        List<string> NameList = new List<string>();

        string enemy = "";
        for (int i=0; i< sWave.Length; i++)
        {
            char c = sWave[i];
            if (c == ' ' && enemy.Length>0)
            {
                NameList.Add(enemy);
                enemy = "";
            }
            else
            {
                enemy += sWave[i];
            }
        }
        if (enemy != "")
        {
            NameList.Add(enemy);
        }
        return NameList.ToArray();
    }
}