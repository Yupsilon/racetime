using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorEnemyBehavior : EnemyBehavior
{
    protected override void Initialize()
    {
        base.Initialize();

        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/EnemyWelcome"));
        poof.transform.position = new Vector2(
            (Camera.main.aspect * Camera.main.orthographicSize / transform.localScale.x) * Data,
            Camera.main.orthographicSize
            );
        poof.transform.localScale = transform.localScale;
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             Camera.main.aspect * Camera.main.orthographicSize * Data / transform.localScale.x,
             Camera.main.orthographicSize +  2,
             transform.position.z
            ) ;
        Body.rotation = 90 * Data;

        base.Spawn();
    }

    void FixedUpdate()
    {
        if (active)
        {
            Body.position += velocity * Time.deltaTime;
            Body.rotation += 3;
        }
    }

    public override float GetCost()
    {
        return transform.localScale.x/3f;
    }
}
