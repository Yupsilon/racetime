using UnityEngine;

public class FinishLineBehavior : EnemyBehavior
{
    protected override void Initialize()
    {
        base.Initialize();
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             0,
             Camera.main.orthographicSize + 2,
             transform.position.z
            );

        base.Spawn();
    }
    public override float GetCost()
    {
        return 0;
    }
}
