using UnityEngine;

public class SpikeEnemyBehavior : EnemyDirectionalBehavior
{
    protected override void Initialize()
    {
        base.Initialize();

        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/EnemyWelcome"));
        poof.transform.position = new Vector2(
             (Camera.main.aspect * Camera.main.orthographicSize) * ChargeDirection,
             Camera.main.orthographicSize * Mathf.Round(Data*10)/10f * .9f
            );
        poof.transform.localScale = transform.localScale.y*Vector3.one;
        poof.transform.rotation = Quaternion.Euler(0, 0, 90 * ChargeDirection);
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             (Camera.main.aspect * Camera.main.orthographicSize + 3f) * ChargeDirection,
             Camera.main.orthographicSize * Mathf.Round(Data * 10) / 10f * .9f,
             transform.position.z
            );

        base.Spawn();
        awaketime = Time.time;
    }

    protected override float GetLifeTime()
    {
        return 4f;
    }

    void FixedUpdate()
    {
        if (active)
        {
            float timedelta = Time.time - awaketime;
            velocity.x = 4 * Mathf.Sin(timedelta % 4f/2f * Mathf.PI) * -ChargeDirection;

            Body.position += velocity * Time.deltaTime;
        }
    }
    public override float GetCost()
    {
        return .66f;
    }
}