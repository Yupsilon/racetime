using UnityEngine;

public class DrunkDriverEnemyBehavior : EnemyDirectionalBehavior
{
    protected override void Initialize()
    {
        base.Initialize();

        GameObject poof = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Effects/EnemyWelcome"));
        poof.transform.position = new Vector2(
            Camera.main.aspect * Camera.main.orthographicSize * Data * .5f,
            Camera.main.orthographicSize
            );
        poof.transform.localScale = transform.localScale;
        poof.transform.localScale = transform.localScale;
    }
    protected override void Spawn()
    {
        transform.position = new Vector3(
             Camera.main.aspect * Camera.main.orthographicSize * Data*.5f,
             Camera.main.orthographicSize + .5f,
             transform.position.z
            );

        base.Spawn();
        awaketime = Time.time;
    }

    void FixedUpdate()
    {
        if (active)
        {
            float timedelta = Time.time - awaketime+.5f;
            velocity.x = velocity.y * Mathf.Sin(timedelta % 2f * Mathf.PI)*2 * ChargeDirection;

            Body.position += velocity * Time.deltaTime;
        }
    }
    public override float GetCost()
    {
        return 1.4f;
    }
}

