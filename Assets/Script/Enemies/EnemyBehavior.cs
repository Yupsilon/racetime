using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBehavior : MonoBehaviour
{
    [SerializeField]
    public bool active = false;
    public Vector2 velocity;
    public float SpawnTime=0;
    public float Data=0;

    protected SpriteRenderer Display;
    protected Collider2D Collision;
    protected Rigidbody2D Body;

    void Awake()
    {
        Display = GetComponent<SpriteRenderer>();
        Collision = GetComponent<Collider2D>();
        Body = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (active) { 
        Body.position += velocity * Time.deltaTime;
        }
    }

    void OnEnable()
    {
        Body.position = Vector2.one * 100;
        Invoke("Initialize", SpawnTime);
    }
    void OnDisable()
    {
        CancelInvoke("Initialize");
        CancelInvoke("Spawn");
        active = false;
        Display.enabled = false;
        Collision.enabled = false;
        Body.simulated = false;
        if (GetComponent<ParticleSystem>()!=null)
        {
            GetComponent<ParticleSystem>().Stop();
        }
    }
    protected virtual void Initialize()
    {
        Invoke("Spawn", .5f);
    }
    protected virtual void Spawn()
    {
        active = true;
        Display.enabled = true;
        Collision.enabled = true;
        Body.simulated = true;
        Invoke("Kill", GetLifeTime());
    }

    public virtual float GetCost()
    {
        return 1f;
    }

    protected virtual float GetLifeTime()
    {
        return Mathf.Abs(16f/velocity.y);
    }

    void Kill()
    {
        CancelInvoke("Kill");
        enabled = false;
    }
}
