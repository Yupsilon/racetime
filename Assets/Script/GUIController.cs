using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    int lap = 0;
    Text Caller;
    Text Timer;

    private void Awake()
    {
        Caller = transform.GetChild(0).GetComponent<Text>();
        Timer = transform.GetChild(1).GetComponent<Text>();        
    }
    public void PrepNewLap(int nlap)
    {
        lap = nlap;
        StartCoroutine("NewLap");
    }
    IEnumerator NewLap()
    {
        Caller.enabled = true;
        Caller.text = "Lap " + lap;
        yield return new WaitForSeconds(GameController.SetupTime);

        Caller.enabled = false;
        Timer.enabled = true;
        float time_remaining = GameController.LapTime;
        Timer.text = (Mathf.Round(time_remaining*10)/10f)+"s";


        while (time_remaining > 0)
        {
            time_remaining -= Time.deltaTime;
            Timer.text = (Mathf.Round(time_remaining * 10) / 10f) + "s";
            yield return new WaitForEndOfFrame();
        }
        Timer.enabled = false;
    }
    private void OnDisable()
    {
        StopAllCoroutines();
        Caller.enabled = false;
        Timer.enabled = false;
    }
}
