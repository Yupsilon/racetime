using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneController : PlayerController
{
    public static Color[] clonecolors = new Color[]
        {
            new Color(1,.6f,.6f),
            new Color(.6f,.95f,.95f),
            new Color(.6f,.95f,.6f),
            new Color(.95f,.9f,.6f)
        };
    protected override void Awake()
    {
        Display = GetComponent<SpriteRenderer>();
        Body = GetComponent<Rigidbody2D>();
        Collision = Instantiate(Resources.Load<GameObject>("Prefabs/Player/CloneHitbox")).GetComponent<Collider2D>();
        Trail = GetComponent<ParticleSystem>();
        Animations = GetComponent<Animator>();
    }
    protected override void OnEnable()
    {
        Display.enabled = true;
        Collision.enabled = true;
        Trail.Play();
        StartCoroutine(SpawnIntoWorld());
    }
    protected override IEnumerator SpawnIntoWorld()
    {
        recordstate = 0;
        Display.color = new Color(MyColor.r, MyColor.g, MyColor.b, .33f);
        Body.simulated = false;
        transform.position = History[0];
        yield return new WaitForSeconds(GameController.SetupTime);
        Display.color = MyColor ;
        Body.simulated = true;
        Collision.enabled = true;
        recordstate++;
        SpawnTime = Time.time;
        defeated = false;
        FTL(true);
    }
    protected override void Update()
    {
        if (recordstate > 0 && recordstate < History.Count)
        {
            if (History[recordstate].z<0)
            {
                Kill();
            }
            if (Time.time >= SpawnTime + History[recordstate].z)
            {
                input = new Vector2(History[recordstate].x, History[recordstate].y);
                recordstate++;
            }
        }
        Collision.transform.position = transform.position;
    }
    public void MimicPlayer(PlayerController other)
    {
        MyColor = clonecolors[other.playerID];
        History = new List<Vector3>();
        History.AddRange(other.History);
    }
    public override void Kill()
    {
        enabled = false;
        defeated = true;
        Explode();
    }
}
