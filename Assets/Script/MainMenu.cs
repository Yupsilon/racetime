using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text Title;
    public Text Subtitle;

    public Text PlayerLabel;
    public Button AddPlayerButton;
    public Button RemovePlayerButton;

    public InputField PlayerInput;
    public Button PlayButton;
    public Button CloseButton;
    private void Awake()
    {
        AddPlayerButton.onClick.AddListener(delegate
        {
            GameController.main.players = Mathf.Min(4, GameController.main.players + 1);
            PlayerLabel.text = GameController.main.players+" ";
        });
        RemovePlayerButton.onClick.AddListener(delegate
        {
            GameController.main.players = Mathf.Max(1, GameController.main.players - 1);
            PlayerLabel.text = GameController.main.players + " ";
        });
        PlayerInput.onEndEdit.AddListener(delegate
        {
            GameController.main.ChangeSeed(PlayerInput.text);
        });
        PlayButton.onClick.AddListener(delegate
        {
            GameController.main.NewLap(true);
        });
        CloseButton.onClick.AddListener(delegate
        {
            Application.Quit();
        });
    }
    public void DeclarePlayerWinner(int winner)
    {
        Title.text = "Game Over";
        Subtitle.text = "Player " + (winner + 1) + " wins!";
    }
    public void DeclarePlayerDefeat()
    {
        int laps = GameController.main.lap - 1;
        Title.text = "Game Over";
        Subtitle.text = "You finished " + laps + " lap"+ (laps == 1 ? "" : "s") + "!";
    }
    public void OnEnable()
    {
        GameController.main.ChangeSeed(GameController.StartingSeeds[Random.Range(0, GameController.StartingSeeds.Length-1)]);
        PlayerLabel.text = GameController.main.players + " ";        
        PlayerInput.text = GameController.main.seed;
    }
}
