using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//sound
    //explode
    //FTL
    //music from NG
//better enemy graphix
//send

public class GameController : MonoBehaviour
{
    public static string[] StartingSeeds = new string[]
    {
        "space",
        "rocket",
        "spaceman",
        "meteor",
        "planet",
        "galaxy",
        "universe",
        "race",
        "lightspeed"
    };

    public static float FinishTime = 3;
    public static float SetupTime = 3;
    public static float LapTime = 30;
    public static float WaveSpawnDelay = 1f;

    static float MinEnemyInterval = 1f;
    static float MaxEnemyInterval = 3f;

    public static GameController main;

    GUIController UI;
    MainMenu MM;

    public int lap = 0;
    public int players = 1;
    public string seed = "space";
    List<PlayerController> Players = new List<PlayerController>();
    void Awake()
    {
        SpawnController = Resources.Load<WaveController>("Prefabs/Enemies/Wave Defines");

        UI = GameObject.FindObjectOfType<GUIController>();
        MM = GameObject.FindObjectOfType<MainMenu>();
        main = this;
    }
    private void Start()
    {
        UI.gameObject.SetActive(false);
        MM.gameObject.SetActive(true);
    }
    void PopulatePlayers(int nPlayers)
    {
        for (int iP = 0; iP < nPlayers; iP++)
        {
            GameObject Player = Instantiate(Resources.Load<GameObject>("Prefabs/Player/Player"));
            Players.Add(Player.GetComponent<PlayerController>());
            Player.GetComponent<PlayerController>().playerID = iP;
            Player.GetComponent<PlayerController>().enabled = true;
        }
    }
    void FreshLap()
    {
        NewLap(false);
    }
    public void NewLap(bool freshStart)
    {
        StopAllCoroutines();
        CancelInvoke("FreshLap");
        CancelInvoke("EndTheGame");
        int active_players = 0;
        if (freshStart)
        {
            ClearAllEntities();
            InitSeed();
            GenerateWaves();
            PopulatePlayers(players);
            active_players = players;
            lap = 0;
            UI.gameObject.SetActive(true);
            MM.gameObject.SetActive(false);
        }
        else
        {
            int player_count = Players.Count;
            for (int iP =0; iP < player_count; iP++)
            {
                PlayerController player = Players[iP];
                if (iP< players)
                {
                    Players.Add(ClonePlayer(player));
                }
                if (!player.defeated)
                {
                    player.enabled = true;
                    active_players++;
                }
            }
        }
        if (CheckGameState())
        {
            foreach (EnemyBehavior enemy in StoredEnemies)
            {
                if (enemy.enabled)
                { 
                enemy.enabled = false;
                }
                enemy.enabled = true;
            }
            lap++;
            UI.PrepNewLap(lap);
            Invoke("EndTheGame", SetupTime+LapTime);
        }
        else
        {
            ClearAllEntities();
        }
    }
    public bool CheckGameState()
    {
        int active_players = 0;
        for (int iP = 0; iP < players; iP++)
        {
            PlayerController player = Players[iP];
            if (!player.defeated)
            {
                active_players++;
            }
        }
        if (players == 1 && active_players == 0)
        {
            DeclareEndGame();
            return false;
        }
        if (players > 1 && active_players == 1)
        {
            DeclarePlayerWinner();
            return false;
        }
        return true;
    }
    void DeclarePlayerWinner()
    {
        UI.gameObject.SetActive(false);
        MM.gameObject.SetActive(true);
        int winner = 0;

        for (int iP = 0; iP < players; iP++)
        {
            PlayerController player = Players[iP];
            if (!player.defeated)
            {
                winner = player.playerID;
            }
        }
        MM.DeclarePlayerWinner(winner);
    }
    void DeclareEndGame()
    {
        UI.gameObject.SetActive(false);
        MM.gameObject.SetActive(true);
        MM.DeclarePlayerDefeat();
    }
    void EndTheGame()
    {
        foreach (PlayerController player in Players)
        {
            player.GetComponent<PlayerController>().EndGame();
        }
        Invoke("FreshLap", FinishTime);
    }
    void ClearAllEntities()
    {
        foreach (PlayerController player in Players)
        {
            GameObject.Destroy(player.gameObject);
        }
        Players.Clear();
        foreach (EnemyBehavior enemy in StoredEnemies)
        {
            GameObject.Destroy(enemy.gameObject);
        }
        StoredEnemies.Clear();

    }
    public CloneController ClonePlayer(PlayerController Spieler)
    {
        GameObject Clone = Instantiate(Resources.Load<GameObject>("Prefabs/Player/Clone"));

        CloneController Controller = Clone.GetComponent<CloneController>();
        Controller.MimicPlayer(Spieler);
        Controller.enabled = true;

        return Controller;
    }

    ////////////////////////
    /// - ENEMY SPAWNING - 
    ////////////////////////

    WaveController SpawnController;
    List<EnemyBehavior> StoredEnemies = new List<EnemyBehavior>();
    public void ChangeSeed(string name)
    {
        seed = name;
    }
    void InitSeed()
        {
            int state = 0;

        for (int c=0; c< seed.Length; c++)
        {
            state += char.ConvertToUtf32(name, c);
        }

        UnityEngine.Random.InitState(11 + 7 * state);
    }
    public void GenerateWaves()
    {
        float currenttime = 0;

        while (currenttime< LapTime-2* WaveSpawnDelay - SetupTime)
        {
            string[] waveData = SpawnController.Wave();

            int wavecount = waveData.Length;
            for (int wave = 0; wave< wavecount; wave++)
            {
                GameObject enemy = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/" + waveData[wave]));
                EnemyBehavior behavior = enemy.GetComponent<EnemyBehavior>();
                behavior.Data = (float)UnityEngine.Random.Range(-1f, 1f);

                float SpawnTime = UnityEngine.Random.Range(MinEnemyInterval, MaxEnemyInterval) * behavior.GetCost() * ((wave == wavecount-1) ? 1 : .5f);
                behavior.SpawnTime = currenttime + SetupTime + SpawnTime + WaveSpawnDelay;
                currenttime += SpawnTime;

                behavior.enabled = false;
                StoredEnemies.Add(behavior);
            }            
        }

        //Finishline
        GameObject FinishLine = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/FinishLine"));
        EnemyBehavior FinishLineBehavior = FinishLine.GetComponent<EnemyBehavior>();
        FinishLineBehavior.SpawnTime = LapTime + WaveSpawnDelay;
        FinishLineBehavior.enabled = false;
        StoredEnemies.Add(FinishLineBehavior);
    }
    public int GetNPlayers()
    {
        return Players.Count;
    }
}
